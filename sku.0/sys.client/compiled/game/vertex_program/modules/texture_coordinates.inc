#if maxTextureCoordinate >= 0
dcl_texcoord0 vTextureCoordinateSet0
#endif

#if maxTextureCoordinate >= 1
dcl_texcoord1 vTextureCoordinateSet1
#endif

#if maxTextureCoordinate >= 2
dcl_texcoord2 vTextureCoordinateSet2
#endif

#if maxTextureCoordinate >= 3
dcl_texcoord3 vTextureCoordinateSet3
#endif

#if maxTextureCoordinate >= 4
dcl_texcoord4 vTextureCoordinateSet4
#endif

#if maxTextureCoordinate >= 5
dcl_texcoord5 vTextureCoordinateSet5
#endif

#if maxTextureCoordinate >= 6
dcl_texcoord6 vTextureCoordinateSet6
#endif

#if maxTextureCoordinate >= 7
dcl_texcoord7 vTextureCoordinateSet7
#endif
