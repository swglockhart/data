// ======================================================================
// inputs:
//
//   none
//
// outputs:
//
//   oPos     = vertex in projection space
//
// ======================================================================

// ----------------------------------------------------------------------
// transform vertex to projection space

m4x4 oPos, vPosition, cObjectWorldCameraProjectionMatrix
